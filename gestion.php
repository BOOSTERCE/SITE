<?php include ('include/header.php'); ?>

<!-- page content -->
<div class="page-content">

    <!-- page content wrapper -->
    <div class="page-content-wrap bg-light bg-texture-1">

        <!-- page content holder -->
        <div class="page-content-holder">

            <div class="quote this-animate" data-animate="fadeInDown">
                <h1 class="text-center">GESTION DES OEUVRES DE SOCIALES (ASC)</h1>
                <br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="text-column text-column-centralized tex-column-icon-lg">
                            <div class="text-column-icon">
                                <span class="fa fa-users"></span>
                            </div>
                            <h4>Gestion des Salariés et des Ayants Droits</h4>
                            <div class="text-column-info">
                                Vous pouvez gérer les fiches de vos salariés, de vos ayants droits et en insérer.
                            </div>
                        </div>
                        <div class="text-column text-column-centralized tex-column-icon-lg">
                            <div class="text-column-icon">
                                <span class="fa fa-money"></span>
                            </div>
                            <h4>Gestion de la Billetterie</h4>
                            <div class="text-column-info">
                                Gérer la billetterie de votre Comité de A à Z en créant des prestations, en les achetant et en surveillant votre stock.
                            </div>
                        </div>
                        <div class="text-column text-column-centralized tex-column-icon-lg">
                            <div class="text-column-icon">
                                <span class="fa fa-file-pdf-o"></span>
                            </div>
                            <h4>Etat de gestion</h4>
                            <div class="text-column-info">
                                Grâce aux différents états imprimables, vous n'avez plus qu'à fournir les éléments imprimés à votre expert.
                            </div>
                        </div>
                        <div class="text-column text-column-centralized tex-column-icon-lg">
                            <div class="text-column-icon">
                                <span class="fa fa-cogs"></span>
                            </div>
                            <h4>Modulable</h4>
                            <div class="text-column-info">
                                BOOSTER CE a été pensé de manière modulable, c'est à dire qu'il fonctionne par modules, il est donc aisé de paramétrer votre espace pour qu'il corresponde à votre comité.
                            </div>
                        </div>
                        <div class="text-column text-column-centralized tex-column-icon-lg">
                            <div class="text-column-icon">
                                <span class="fa fa-scissors"></span>
                            </div>
                            <h4>Départager</h4>
                            <div class="text-column-info">
                                Les parties "gestion des ASC" et du "budget de fonctionnement" sont séparées comme le veut la législation en vigueur de 2015.
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- ./page content holder -->
    </div>
    <!-- ./page content wrapper -->
    <!-- page content wrapper -->
    <div class="page-content-wrap bg-dark">
        <!-- page content holder -->
        <div class="page-content-holder padding-v-20">

            <div class="text-center">
                <h2 style="color: white;">N'hésitez pas !</h2> <a href="contact.php" class="btn btn-primary btn-xl"><span class="fa fa-envelope"></span> Contactez-nous</a>
            </div>

        </div>
        <!-- ./page content holder -->
    </div>
    <!-- ./page content wrapper -->

</div>
<!-- ./page content -->

<!-- page footer -->
<div class="page-footer">

    <!-- page footer wrap -->
    <div class="page-footer-wrap bg-dark-gray">
        <!-- page footer holder -->
        <div class="page-footer-holder page-footer-holder-main">

            <div class="row">
                <div class="col-md-9">
                    <h3>ILS NOUS FONT CONFIANCE</h3>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="img/comite/barilla.png" class="img-responsive" />
                        </div>
                        <div class="col-md-3">
                            <img src="img/comite/csl.jpg" class="img-responsive" />
                        </div>
                        <div class="col-md-3">
                            <img src="img/comite/marie.png" class="img-responsive" />
                        </div>
                        <div class="col-md-3">
                            <img src="img/comite/mlp.jpg" class="img-responsive" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="img/comite/rexam.jpg" class="img-responsive" />
                        </div>
                        <div class="col-md-3">
                            <img src="img/comite/slts.png" class="img-responsive" />
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- ./page footer holder -->
    </div>
    <!-- ./page footer wrap -->

    <!-- page footer wrap -->
    <div class="page-footer-wrap bg-darken-gray">
        <!-- page footer holder -->
        <div class="page-footer-holder">

            <!-- copyright -->
            <div class="copyright">
                &copy; 2017 BOOSTER CE - Tout droits réserver |
                <!--<span class="pull-right"><a href="pc.php">Politique de confidentialité</a></span>-->
            </div>
            <!-- ./copyright -->


        </div>
        <!-- ./page footer holder -->
    </div>
    <!-- ./page footer wrap -->

</div>
<!-- ./page footer -->

</div>
<!-- ./page container -->

<!-- page scripts -->
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>

<script type="text/javascript" src="js/plugins/mixitup/jquery.mixitup.js"></script>
<script type="text/javascript" src="js/plugins/appear/jquery.appear.js"></script>

<script type="text/javascript" src="js/plugins/revolution-slider/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="js/plugins/revolution-slider/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript" src="js/actions.js"></script>
<script type="text/javascript" src="js/slider.js"></script>
<!-- ./page scripts -->
</body>
</html>






