<?php include ('include/header.php'); ?>
            
            <!-- page content -->
            <div class="page-content">


                <!-- revolution slider -->
                <div class="banner-container">
                    <div class="banner">

                        <ul>

                            <li data-transition="fade" data-slotamount="1" data-masterspeed="500"  data-saveperformance="on">
                                <img src="img/backgrounds/bg-1.jpg" />

                                <div class="tp-caption black_thin_34 customin tp-resizeme rs-parallaxlevel-0"
                                     data-x="0"
                                     data-y="50"
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="500"
                                     data-start="1200"
                                     data-easing="Back.easeOut"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 4;">
                                    <div style="font-size: 20px; font-weight: bold; font-style: italic; color: #00C0FF; font-family: arial, sans-serif;">Propulsé par</div><br>
                                </div>
                                <div class="tp-caption lft customout rs-parallaxlevel-0"
                                     data-x="45"
                                     data-y="100"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="700"
                                     data-start="1200"
                                     data-easing="Power3.easeInOut"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 3;">
                                    <img src="assets/slider-layers/slide1/logo_synergies.jpg" alt="Atlant"/>
                                </div>
                                <div class="tp-caption lft customout rs-parallaxlevel-0"
                                     data-x="590"
                                     data-y="130"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="700"
                                     data-start="1200"
                                     data-easing="Power3.easeInOut"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 3;">
                                    <img src="assets/slider-layers/slide1/imac1.png" alt="Atlant"/>
                                </div>
                                <div class="tp-caption lft customout rs-parallaxlevel-0"
                                     data-x="550"
                                     data-y="230"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="700"
                                     data-start="1500"
                                     data-easing="Power3.easeInOut"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 3;">
                                    <img src="assets/slider-layers/slide1/ipad.png" alt="Atlant"/>
                                </div>
                                <div class="tp-caption lft customout rs-parallaxlevel-0"
                                     data-x="850"
                                     data-y="250"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="700"
                                     data-start="1900"
                                     data-easing="Power3.easeInOut"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 3;">
                                    <img src="assets/slider-layers/slide1/iphone.png" alt="Atlant"/>
                                </div>
                                <div class="tp-caption lft customout rs-parallaxlevel-0"
                                     data-x="800"
                                     data-y="0"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="700"
                                     data-start="2100"
                                     data-easing="Power3.easeInOut"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 3;">
                                    <img src="assets/slider-layers/slide1/sys-responsive.png" alt="Atlant"/>
                                </div>
                                <div class="tp-caption lft customout rs-parallaxlevel-0"
                                     data-x="180"
                                     data-y="420"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="700"
                                     data-start="2300"
                                     data-easing="Power3.easeInOut"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 3;">
                                    <img src="assets/slider-layers/slide1/dispo_support.png" alt="Atlant"/>
                                </div>

                                <div class="tp-caption black_thin_34 customin tp-resizeme rs-parallaxlevel-0"
                                     data-x="0"
                                     data-y="350"
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="500"
                                     data-start="1200"
                                     data-easing="Back.easeOut"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 4;">
                                    <div style="font-size: 30px; font-weight: bold; color: black; font-family: arial, sans-serif;">DESIGN EPUR&Eacute;</div><br>
                                    <div style="font-family: arial, sans-serif; font-size: 20px; color: black;">BoosterCE s'adapte à tous les supports.<br> Ordinateur, Iphone, Ipad, Mobile, etc...</div>
                                </div>


                            </li>

                        </ul>

                    </div>
                </div>
                <!-- ./revolution slider -->

                <!-- page content wrapper -->                
                <div class="page-content-wrap bg-light bg-texture-1">
                    
                    <div class="divider"><div class="box"><span class="fa fa-angle-down"></span></div></div>                    
                    
                    <!-- page content holder -->
                    <div class="page-content-holder">                        
                        
                        <div class="quote this-animate" data-animate="fadeInDown">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="text-column text-column-centralized tex-column-icon-lg this-animate" data-animate="fadeInTop">
                                        <div class="text-column-icon">
                                            <span class="fa fa-database"></span>
                                        </div>
                                        <h4 style="text-align: center;">BOOSTER CE - Gestion de CE</h4>
                                        <div class="text-column-info">
                                            Suivez l'évolution détaillée de votre C.E dans le respect des obligations nouvelles applicables dès le 1er Janvier 2015.<br>
                                            <ul>
                                                <li>Etablissement d'un bilan d'activité</li>
                                                <li>Détail des prestations du C.E</li>
                                                <li>Détail des Abondements</li>
                                                <li>Alertes sur Stocks</li>
                                                <li>Etc...</li>
                                            </ul>
                                            Et bien sûr comptabilisation des opérations
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="text-column text-column-centralized tex-column-icon-lg this-animate" data-animate="fadeInTop">
                                        <div class="text-column-icon">
                                            <span class="fa fa-euro"></span>
                                        </div>
                                        <h4 style="text-align: center;">BOOSTER CE - Comptabilité de CE</h4>
                                        <div class="text-column-info">
                                            <p>Le principe incontournable de la séparation des budgets vous impose la tenue d'une comptabilité séparée:<br>
                                            Budget de Fonctionnement / Budget des Oeuvres Sociales</p>
                                            <br>
                                            <p>BOOSTER CE vous permet de suivre la comptabilité de votre CE d'une manière autonomne</p>
                                            <br>
                                            <p>Interface ergonomique et responsive (application pouvant être lue sur les terminaux mobiles de dernière génération).</p>
                                            <br>
                                            <p>Tous les états sont induits: Balance, Compte de résultat, Bilan et annexes...</p>
                                            <br>
                                            <p>La création de cette application est supervisée par un expert qui à mis en place une méthode d'enregistrement adaptée à tous.</p>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="text-column text-column-centralized tex-column-icon-lg this-animate" data-animate="fadeInTop">
                                        <div class="text-column-icon">
                                            <span class="fa fa-link"></span>
                                        </div>
                                        <h4 style="text-align: center;">Module Complémentaire</h4>
                                        <div class="text-column-info">
                                            <p>Le coeur du programme est entièrement paramétrable, il est donc aisé pour l'équipe de BOOSTERCE de concevoir votre module pour parfaire votre gestion.</p>
                                            <p><i>Ex: Module de remboursement sur facture, Prise en Charge du solde salarié, Alerte sur Stock, etc...</i></p>
                                            <p>Une liaison entre les Inter-CE et le logiciel BOOSTER CE est possible (sous reserve de spécificités techniques).</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                        
                    </div>
                    <!-- ./page content holder -->
                </div>
                <!-- ./page content wrapper -->                    

            </div>
            <!-- ./page content -->
            
            <!-- page footer -->
            <div class="page-footer">
                
                <!-- page footer wrap -->
                <div class="page-footer-wrap bg-dark-gray">
                    <!-- page footer holder -->
                    <div class="page-footer-holder page-footer-holder-main">
                                                
                        <div class="row">

                            <div class="col-md-9">
                                <h3>ILS NOUS FONT CONFIANCE</h3>
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="img/comite/barilla.png" class="img-responsive" />
                                    </div>
                                    <div class="col-md-3">
                                        <img src="img/comite/csl.jpg" class="img-responsive" />
                                    </div>
                                    <div class="col-md-3">
                                        <img src="img/comite/marie.png" class="img-responsive" />
                                    </div>
                                    <div class="col-md-3">
                                        <img src="img/comite/mlp.jpg" class="img-responsive" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="img/comite/rexam.jpg" class="img-responsive" />
                                    </div>
                                    <div class="col-md-3">
                                        <img src="img/comite/slts.png" class="img-responsive" />
                                    </div>
                                </div>
                            </div>

                            
                        </div>
                        
                    </div>
                    <!-- ./page footer holder -->
                </div>
                <!-- ./page footer wrap -->
                
                <!-- page footer wrap -->
                <div class="page-footer-wrap bg-darken-gray">
                    <!-- page footer holder -->
                    <div class="page-footer-holder">
                        
                        <!-- copyright -->
                        <div class="copyright">
                            &copy; 2017 BOOSTER CE - Tout droits réserver |
                            <!--<span class="pull-right"><a href="pc.php">Politique de confidentialité</a></span>-->
                        </div>
                        <!-- ./copyright -->

                        
                    </div>
                    <!-- ./page footer holder -->
                </div>
                <!-- ./page footer wrap -->
                
            </div>
            <!-- ./page footer -->
            
        </div>        
        <!-- ./page container -->
        
        <!-- page scripts -->
        <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>
        
        <script type="text/javascript" src="js/plugins/mixitup/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/plugins/appear/jquery.appear.js"></script>
        
        <script type="text/javascript" src="js/plugins/revolution-slider/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="js/plugins/revolution-slider/jquery.themepunch.revolution.min.js"></script>
        
        <script type="text/javascript" src="js/actions.js"></script>
        <script type="text/javascript" src="js/slider.js"></script>
        <!-- ./page scripts -->
    </body>
</html>






