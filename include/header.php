<!DOCTYPE html>
<html lang="en">
<head>
    <!-- META SECTION -->
    <title>BOOSTER CE | Logiciel de gestion et de comptabilitÚ pour les ComitÚs</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- END META SECTION -->

    <link rel="stylesheet" type="text/css" href="css/revolution-slider/extralayers.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/revolution-slider/settings.css" media="screen" />

    <link rel="stylesheet" type="text/css" href="css/styles.css" media="screen" />

</head>
<body>
<!-- page container -->
<div class="page-container">

    <!-- page header -->
    <div class="page-header">

        <!-- page header holder -->
        <div class="page-header-holder">

            <!-- page logo -->
            <div class="logo">
                <a href="index.php">BOOSTER CE | LOGICIEL DE GESTION ET DE COMPTABILITE DE COMITE</a>
            </div>
            <!-- ./page logo -->

            <!-- nav mobile bars -->
            <div class="navigation-toggle">
                <div class="navigation-toggle-button"><span class="fa fa-bars"></span></div>
            </div>
            <!-- ./nav mobile bars -->

            <!-- navigation -->
            <ul class="navigation">
                <li>
                    <a href="index.php">Accueil</a>
                </li>
                <li>
                    <a href="#">Logiciel</a>
                    <ul>
                        <li><a href="gestion.php">Gestion</a></li>
                        <li><a href="compta.php">Compta</a></li>
                    </ul>
                </li>
                <li><a href="contact.php">Contact</a></li>
            </ul>
            <!-- ./navigation -->


        </div>
        <!-- ./page header holder -->

    </div>
    <!-- ./page header -->