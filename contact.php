<?php include ('include/header.php'); ?>

<!-- page content -->
<div class="page-content">

    <!-- page content wrapper -->
    <div class="page-content-wrap bg-light">
        <!-- page content holder -->
        <div class="page-content-holder no-padding">
            <!-- page title -->
            <div class="page-title">
                <h1>NOUS CONTACTER</h1>
            </div>
            <!-- ./page title -->
        </div>
        <!-- ./page content holder -->
    </div>
    <!-- ./page content wrapper -->


    <!-- page content wrapper -->
    <div class="page-content-wrap">

        <div id="google-map" style="width: 100%; height: 300px;"></div>

    </div>
    <!-- ./page content wrapper -->

    <!-- page content wrapper -->
    <div class="page-content-wrap">

        <div class="divider"><div class="box"><span class="fa fa-angle-down"></span></div></div>

        <!-- page content holder -->
        <div class="page-content-holder">
            <?php
            if(isset($_POST['contact']) && $_POST['contact'] == 'Valider')
            {
                $nom_comite = $_POST['nom_comite'];
                $adresse_comite = $_POST['adresse_mail_comite'];
                $telephone = $_POST['num_telephone'];
                $sujet = htmlentities($_POST['sujet']);
                $message = htmlentities($_POST['message']);

$mail = 'cdtgestion@free.fr'; // Déclaration de l'adresse de destination.
if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui rencontrent des bogues.
{
    $passage_ligne = "\r\n";
}
else
{
    $passage_ligne = "\n";
}
//=====Déclaration des messages au format texte et au format HTML.
$message_txt = "Salut à tous, voici un e-mail envoyé par un script PHP.";
$message_html = "<html><head></head><body>Nouveau message de contact d'un comité:<br><br>Nom du comité: ".$nom_comite."<br>Adresse Mail: ".$adresse_comite."<br>Numéro de Téléphone: ".$telephone."<br>Sujet: ".html_entity_decode($sujet)."<br>Message: ".html_entity_decode($message)."</body></html>";
//==========

//=====Création de la boundary
$boundary = "-----=".md5(rand());
//==========

//=====Définition du sujet.
$sujet = "BOOSTER CE - DEMANDE DE CONTACT";
//=========

//=====Création du header de l'e-mail.
$header = "From: \"BOOSTERCE\"<no-reply@boosterce.com>".$passage_ligne;
$header.= "MIME-Version: 1.0".$passage_ligne;
$header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
//==========

//=====Création du message.
$message = $passage_ligne."--".$boundary.$passage_ligne;
//=====Ajout du message au format texte.
$message.= "Content-Type: text/plain; charset=\"ISO-8859-1\"".$passage_ligne;
$message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
$message.= $passage_ligne.$message_txt.$passage_ligne;
//==========
$message.= $passage_ligne."--".$boundary.$passage_ligne;
//=====Ajout du message au format HTML
$message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
$message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
$message.= $passage_ligne.$message_html.$passage_ligne;
//==========
$message.= $passage_ligne."--".$boundary."--".$passage_ligne;
$message.= $passage_ligne."--".$boundary."--".$passage_ligne;
//==========

//=====Envoi de l'e-mail.
$envoie_mail = mail($mail,$sujet,$message,$header);
//==========
if($envoie_mail === TRUE)
{
    ?>
    <div class="row">
        <div class="col-md-7 this-animate" data-animate="fadeInLeft">

            <div class="text-column">
                <h4>Contactez-nous</h4>
                <div class="text-column-info">
                    Merci, nous vous recontacterons dans les plus brefs délais
                </div>
            </div>

        </div>
        <div class="col-md-5 this-animate" data-animate="fadeInRight">

            <div class="text-column text-column-centralized">
                <div class="text-column-icon">
                    <span class="fa fa-home"></span>
                </div>
                <h4>NOTRE BUREAU</h4>
                <div class="text-column-info">
                    <p><strong><span class="fa fa-map-marker"></span> Adresse: </strong> 3 Avenue de la grande roche, 85470 BRETIGNOLLES SUR MER</p>
                    <p><strong><span class="fa fa-phone"></span> Téléphone: </strong> 02 51 33 52 79</p>
                    <p><strong><span class="fa fa-mobile"></span> Portable: </strong> 07 68 16 25 51</p>
                    <p><strong><span class="fa fa-envelope"></span> E-mail: </strong> <a href="#">vhs24@free.fr</a></p>
                </div>
            </div>

        </div>
    </div>
            <?php
}


            }
            ?>

            <div class="row">
                <div class="col-md-7 this-animate" data-animate="fadeInLeft">

                    <div class="text-column">
                        <h4>Contactez-nous</h4>
                        <div class="text-column-info">
                            Si vous avez des questions ou besoin d'information sur le logiciel BOOSTER CE, n'hésitez pas à nous envoyer un message, nous vous répondrons dans les plus brefs délais.
                        </div>
                    </div>
                    <form action="contact.php" method="post">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Nom de votre Comité <span class="text-hightlight">*</span></label>
                                    <input type="text" class="form-control" name="nom_comite"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Votre adresse Mail <span class="text-hightlight">*</span></label>
                                    <input type="text" class="form-control" name="adresse_mail_comite"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Téléphone <span class="text-hightlight">*</span></label>
                                    <input type="text" class="form-control" name="num_telephone"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Sujet <span class="text-hightlight">*</span></label>
                                    <input type="text" class="form-control" name="sujet"/>
                                </div>
                                <div class="form-group">
                                    <label>Votre Message <span class="text-hightlight">*</span></label>
                                    <textarea class="form-control" rows="8" name="message"></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary btn-lg pull-right" name="contact" value="Valider">Envoyer</button>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="col-md-5 this-animate" data-animate="fadeInRight">

                    <div class="text-column text-column-centralized">
                        <div class="text-column-icon">
                            <span class="fa fa-home"></span>
                        </div>
                        <h4>NOTRE BUREAU</h4>
                        <div class="text-column-info">
                            <p><strong><span class="fa fa-map-marker"></span> Adresse: </strong> 10 rue du Colisée, 75008 PARIS</p>
                            <p><strong><span class="fa fa-phone"></span> Téléphone: </strong> 09 50 02 68 95</p>
                            <p><strong><span class="fa fa-mobile-phone"></span> Portable: </strong> 06 14 02 09 86</p>
                            <p><strong><span class="fa fa-envelope"></span> E-mail: </strong> <a href="#">contact@boosterce.com</a></p>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- ./page content holder -->
    </div>
    <!-- ./page content wrapper -->

</div>
<!-- ./page content -->

<!-- page footer -->
<div class="page-footer">

    <!-- page footer wrap -->
    <div class="page-footer-wrap bg-dark-gray">
        <!-- page footer holder -->
        <div class="page-footer-holder page-footer-holder-main">

            <div class="row">
                <div class="col-md-9">
                    <h3>ILS NOUS FONT CONFIANCE</h3>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="img/comite/barilla.png" class="img-responsive" />
                        </div>
                        <div class="col-md-3">
                            <img src="img/comite/csl.jpg" class="img-responsive" />
                        </div>
                        <div class="col-md-3">
                            <img src="img/comite/marie.png" class="img-responsive" />
                        </div>
                        <div class="col-md-3">
                            <img src="img/comite/mlp.jpg" class="img-responsive" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="img/comite/rexam.jpg" class="img-responsive" />
                        </div>
                        <div class="col-md-3">
                            <img src="img/comite/slts.png" class="img-responsive" />
                        </div>
                    </div>
                </div>


            </div>

        </div>
        <!-- ./page footer holder -->
    </div>
    <!-- ./page footer wrap -->

    <!-- page footer wrap -->
    <div class="page-footer-wrap bg-darken-gray">
        <!-- page footer holder -->
        <div class="page-footer-holder">

            <!-- copyright -->
            <div class="copyright">
                &copy; 2017 BOOSTERCE - Tout droits réserver |
                <!--<span class="pull-right"><a href="pc.php">Politique de confidentialité</a></span>-->
            </div>
            <!-- ./copyright -->


        </div>
        <!-- ./page footer holder -->
    </div>
    <!-- ./page footer wrap -->

</div>
<!-- ./page footer -->

</div>
<!-- ./page container -->

<!-- page scripts -->
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>

<script type="text/javascript" src="js/plugins/mixitup/jquery.mixitup.js"></script>
<script type="text/javascript" src="js/plugins/appear/jquery.appear.js"></script>

<script type="text/javascript" src="js/plugins/revolution-slider/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="js/plugins/revolution-slider/jquery.themepunch.revolution.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript">

    var mapCords = new google.maps.LatLng(48.8706384, 2.3080191999999897);
    var mapOptions = {zoom: 14,center: mapCords, mapTypeId: google.maps.MapTypeId.ROADMAP}
    var map = new google.maps.Map(document.getElementById("google-map"), mapOptions);

    var cords = new google.maps.LatLng(48.8706384, 2.3080191999999897);
    var marker = new google.maps.Marker({position: cords,
            map: map,
            title: "Marker 1",
            icon: 'http://aqvatarius.com/development/atlant-frontend/img/map-marker.png'}
    );

</script>
<script type="text/javascript" src="js/actions.js"></script>
<script type="text/javascript" src="js/slider.js"></script>
<!-- ./page scripts -->
</body>
</html>






